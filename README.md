# Sports Spatialverse #



### What is Sports Spatialverse? ###

**Sports Spatialverse** is an ecosystem that encompasses real and virtual interaction with sports and physical activity.  

The term “**spatialverse**” refers to a combination of virtual reality (VR), augmented reality (AR), and spatial computing.

**Sports Spatialverse** is comprised of sports venue digital twins, a sports-oriented social network, an e-commerce marketplace, web sites, mobile apps, and VR/AR games.

Digital twins of real and imagined sports arenas will allow fans that are unable to attend a real venue (due to distance or a fear of infection) to  interact with and view games in real time or with delayed recorded access as though they were there.

**Benten** is a crypto social network based on blockchain technology within an interactive 3D environment.
**Benten** is similar to Twitter with posts, profiles, and follows.  
**DAIKO** is the native cryptocurrency of the **Benten** network. 

**Spatial Bazaar** is a marketplace to obtain digital assets and tickets to real sporting events as well as virtual access to these and other events. 